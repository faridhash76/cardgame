import React from 'react';
import Deck from "./Components/Deck";


function App() {
  return (
    <div className="App">
      <Deck/>
    </div>
  );
}

export default App;
